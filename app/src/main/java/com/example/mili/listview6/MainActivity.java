package com.example.mili.listview6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lsCountry;
    ArrayList<list>arrayCountry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addControl();
    }

    private void addControl() {
        lsCountry = (ListView) findViewById(R.id.lstCountry);
        arrayCountry = new ArrayList<list>();
        arrayCountry.add(new list("Việt Nam",R.drawable.v1));
        arrayCountry.add(new list("Việt Nam",R.drawable.v2));
        arrayCountry.add(new list("Việt Nam",R.drawable.v3));
        arrayCountry.add(new list("Việt Nam",R.drawable.v4));
        arrayCountry.add(new list("Việt Nam",R.drawable.v5));
        arrayCountry.add(new list("Việt Nam",R.drawable.v6));
        arrayCountry.add(new list("Việt Nam",R.drawable.v1));
        arrayCountry.add(new list("Việt Nam",R.drawable.v2));
        arrayCountry.add(new list("Việt Nam",R.drawable.v3));
        arrayCountry.add(new list("Việt Nam",R.drawable.v4));
        arrayCountry.add(new list("Việt Nam",R.drawable.v5));
        arrayCountry.add(new list("Việt Nam",R.drawable.v6));
        AdapterCountry adapter = new AdapterCountry(
                MainActivity.this,R.layout.list,arrayCountry// from source in adapter
        );

        lsCountry.setAdapter(adapter);//from adap in list


    }
}
