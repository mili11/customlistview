package com.example.mili.listview6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by MILI on 9/13/2018.
 */

public class AdapterCountry extends BaseAdapter {

    private Context context;
    private int layout;
    private List<list> country;

    public AdapterCountry(Context context, int layout, List<list> country) {
        this.context = context;
        this.layout = layout;
        this.country = country;
    }

    @Override
    public int getCount() {///số dòng trên list view
        return country.size();//lấy ptu trong Country
    }

    @Override
    public Object getItem(int i) {//đối tượng
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {//trả về mỗi dòng trên item
        //lấy giao diện custom, hiển thị trên item, context màn hình mình custom
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout,null);
        //ánh xạ và gán giá trị
        TextView txtName = (TextView) view.findViewById(R.id.textView);
        ImageView hinh = (ImageView) view.findViewById(R.id.imageView);

        list ls = country.get(i);
        txtName.setText(country.get(i).getName());
        hinh.setImageResource(country.get(i).getHinh());


        return view;
    }
}
