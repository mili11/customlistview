package com.example.mili.listview6;

/**
 * Created by MILI on 9/12/2018.
 */

public class list {
    private String Name;

    private int hinh;
    public list(String name, int hinh) {
        Name = name;
        this.hinh = hinh;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getHinh() {
        return hinh;
    }

    public void setHinh(int hinh) {
        this.hinh = hinh;
    }
}
